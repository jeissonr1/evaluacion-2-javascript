//menu
function cambiarclase() {
    let menumob = document.getElementById('menu');
    menumob.classList.toggle('menumobile-open');
    let menuOpen = document.getElementById('menu-mobile');
    menuOpen.classList.toggle('menu-open');
}

//Select del menú
const selectmenu = document.querySelectorAll('.Class-m')
const li = document.querySelectorAll('.li')
const bloque = document.querySelectorAll('.bloque')

selectmenu.forEach((cadamenu, e) => {
    selectmenu[e].addEventListener('click', () => {

        /* //Leer Scroll        
                window.addEventListener("scroll", (h) => {
                    let scrol1 = window.pageYOffset;
                    console.log(scrol1)
                }) */
        //Deslizamiento de menú superior
        window.scrollTo({
            behavior: "smooth",
            top: 690,
        })


        selectmenu.forEach((cadamenu, e) => {
            selectmenu[e].classList.remove('activo')
            li[e].classList.remove('activo')
            bloque[e].classList.remove('activo')
        })

        selectmenu[e].classList.add('activo')
        li[e].classList.add('activo')
        bloque[e].classList.add('activo')
    })
})

//Buscador 
const texto = document.querySelector('#buscar-texto');
const imgbuscador = document.querySelector('#img-buscador');
const filtrar = () => {
    console.log(texto.value);
}
imgbuscador.addEventListener('click', filtrar)


//cambio de columnas por grids
const gridlineal = document.querySelector('.grid-lineal')
gridlineal.addEventListener('click', () => {
    document.querySelectorAll(".bloquess").forEach(element => {
        element.style.columns = "1"
    });
})

const gridbox = document.querySelector('.grid-box')
gridbox.addEventListener('click', () => {
    document.querySelectorAll(".bloquess").forEach(element => {
        element.style.columns = "3"

    });
})


/* inttento de Slider 
const slider = () => {
    let sliders = document.querySelector('.ul');

    if (sliders) {
        const sliderlinks = tns({
            container: sliders,
            items: 5,
            "responsive": {
                "768": {
                    "loop": true,
                    "gutter": 10,
                    "items": 3,
                    "center": true,
                    "autoHeight": false,
                    "slideBy": 1
                }
            },
            slideBy: 1,
            gutter: 10,
            center: false,
            autoplay: false,
            loop: true,
            controls: false,
            controlsPosition: 'bottom',
            nav: false,
            navPosition: 'bottom',
        });

    }
}
 */

//cambio y asignacion de clase activo en las tabs seleccionadas
li.forEach((cadaLi, i) => {
    li[i].addEventListener('click', () => {

        li.forEach((cadaLi, i) => {
            li[i].classList.remove('activo')
            bloque[i].classList.remove('activo')
        })

        li[i].classList.add('activo')
        bloque[i].classList.add('activo')
    })
})

//traer gifs
//All
let url = 'https://api.giphy.com/v1/gifs/trending?limit=45&api_key=9SxK24Lj7usjYZKKsUdJqjdo2IXj3YzE'
fetch(url)
    .then(response => response.json())
    .then(data => mostrarData(data))
    .catch(error => console.log(error))

const mostrarData = (data) => {
    console.log(data)
    let body = ''
    for (let i = 0; i < data.data.length; i++) {
        body += `<img class="gif" src = ${data.data[i].images.downsized_medium.url}>`
    }
    document.getElementById('bloque1').innerHTML = body
}

//Branding
let urlb = 'https://api.giphy.com/v1/gifs/search?q=Marca&rating=G&limit=45&api_key=9SxK24Lj7usjYZKKsUdJqjdo2IXj3YzE'
fetch(urlb)
    .then(response => response.json())
    .then(data => mostrarData2(data))
    .catch(error => console.log(error))

const mostrarData2 = (data) => {
    console.log(data)
    let body = ''
    for (let i = 0; i < data.data.length; i++) {
        body += `<img class="gif" src = ${data.data[i].images.downsized_medium.url}>`
    }
    document.getElementById('bloque2').innerHTML = body
}

//Web
let urlw = 'https://api.giphy.com/v1/gifs/search?q=Web&limit=45&api_key=9SxK24Lj7usjYZKKsUdJqjdo2IXj3YzE'
fetch(urlw)
    .then(response => response.json())
    .then(data => mostrarData3(data))
    .catch(error => console.log(error))

const mostrarData3 = (data) => {
        console.log(data)
        let body = ''
        for (let i = 0; i < data.data.length; i++) {
            body += `<img class="gif" src = ${data.data[i].images.downsized_medium.url}>`
        }
        document.getElementById('bloque3').innerHTML = body
    }
    //Photography
let urlp = 'https://api.giphy.com/v1/gifs/search?q=foto&limit=45&api_key=9SxK24Lj7usjYZKKsUdJqjdo2IXj3YzE'
fetch(urlp)
    .then(response => response.json())
    .then(data => mostrarData4(data))
    .catch(error => console.log(error))

const mostrarData4 = (data) => {
        console.log(data)
        let body = ''
        for (let i = 0; i < data.data.length; i++) {
            body += `<img class="gif" src = ${data.data[i].images.downsized_medium.url}>`
        }
        document.getElementById('bloque4').innerHTML = body
    }
    //APP
let urla = 'https://api.giphy.com/v1/gifs/search?q=App&limit=45&api_key=9SxK24Lj7usjYZKKsUdJqjdo2IXj3YzE'
fetch(urla)
    .then(response => response.json())
    .then(data => mostrarData5(data))
    .catch(error => console.log(error))

const mostrarData5 = (data) => {
    console.log(data)
    let body = ''
    for (let i = 0; i < data.data.length; i++) {
        body += `<img class="gif" src = ${data.data[i].images.downsized_medium.url}>`
    }
    document.getElementById('bloque5').innerHTML = body
}

//traer mas gifs
const showmore = (data, boton) => {
    let block;
    let contentImg = [];
    setTimeout(() => {
        block = document.querySelectorAll(`${data} > img`);
        //console.log(block)
        block.forEach(e => {
            let elementActive = e.classList.contains('active');

            if (elementActive == false) {
                contentImg.push(e)
            }
        });

        if (contentImg.length > 1) {
            for (let i = 0; i < 9; i++) {
                contentImg[i].classList.add('active');
            }
        }
        //console.log(contentImg.length)
        if (contentImg.length === 0) {
            document.querySelector(`${boton}`).style.display = 'none';
        }
    }, 1000);
}

showmore('#bloque1', '#showMore1');
const showmore1 = document.querySelector('#showMore1');
if (showmore1) {
    showmore1.addEventListener('click', (e) => {
        showmore('#bloque1', '#showMore1')
    })
}

showmore('#bloque2', '#showMore2');
const showmore2 = document.querySelector('#showMore2');
if (showmore2) {
    showmore2.addEventListener('click', (e) => {
        showmore('#bloque2', '#showMore2')
    })
}
showmore('#bloque3', '#showMore3');
const showmore3 = document.querySelector('#showMore3');
if (showmore3) {
    showmore3.addEventListener('click', (e) => {
        showmore('#bloque3', '#showMore3')
    })
}
showmore('#bloque4', '#showMore4');
const showmore4 = document.querySelector('#showMore4');
if (showmore4) {
    showmore4.addEventListener('click', (e) => {
        showmore('#bloque4', '#showMore4')
    })
}
showmore('#bloque5', '#showMore5');
const showmore5 = document.querySelector('#showMore5');
if (showmore5) {
    showmore5.addEventListener('click', (e) => {
        showmore('#bloque5', '#showMore5')
    })
}